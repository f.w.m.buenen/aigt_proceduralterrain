﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    [SerializeField]
    private float cellScale;

    [SerializeField]
    private int subdivisions;

    private Mesh mesh;

    private Vector3[] vertices;
    private int[] triangles;

    // Start is called before the first frame update
    void Start()
    {
        // if(subdivisions > 256)
        // {
        //     Debug.LogError("can't have more than 65k verts, subdivisions should be <= 256");
        //     return;
        // }
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        CreateTerrainMesh();
    }

    private void CreateTerrainMesh()
    {
        vertices = new Vector3[(subdivisions + 1) * (subdivisions + 1)];
        int i = 0;
        for (int z = 0; z <= subdivisions; z++)
        {
            for (int x = 0; x <= subdivisions; x++)
            {
                vertices[i] = new Vector3(x * cellScale, 0, z * cellScale);
                i++;
            }
        }

        triangles = new int[subdivisions * subdivisions * 6];

        int vert = 0;
        int tris = 0;
        for(int z = 0; z < subdivisions; z++)
        {
            for (int x = 0; x < subdivisions; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + subdivisions + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + subdivisions + 1;
                triangles[tris + 5] = vert + subdivisions + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }

    }

    public void UpdateMesh(Heightmap heightmap, Texture2D colormap)
    {
        mesh.Clear();

        if (heightmap != null)
        {
            int scale = heightmap.getSize() / subdivisions;
            for (int z = 0; z < subdivisions; z++)
                for (int x = 0; x < subdivisions; x++){
                    vertices[z * (subdivisions+1) + x].y = heightmap.GetPixel(x*scale, z*scale) * 30;
                }
        }

        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        GetComponent<ProceduralShader>().SetColors(mesh, heightmap, colormap, subdivisions, vertices.Length);
    }

    private void OnDrawGizmos()
    {
        //for(int i = 0; i < vertices.Length; i++)
        //{
        //    //Gizmos.DrawSphere(vertices[i], 0.1f);
        //}
    }
}
