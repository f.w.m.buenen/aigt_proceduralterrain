﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverAgent : MonoBehaviour
{
    private static System.Random rng = new System.Random();  

    private Vector2 pos;

    private float cliffHeight = 0.6f;

    private Vector2 origDir;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool isLand(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) >= 0.01f;
    }

    bool isSea(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) < 0.01f;
    }

    private Vector2 FindGradient(Heightmap heightmap, Vector2Int intPos)
    {
        Vector2 gradDir = new Vector2(0, 0);
        
        // Find the gradient direction
        float minHeight = 100;
        for (int dx = -1; dx <= 1; dx++)
        {
            for (int dy = -1; dy <= 1; dy++)
            {
                if (dx == 0 && dy == 0) continue;

                Vector2Int testPos = new Vector2Int(intPos.x + dx, intPos.y + dy);
                float testHeight = heightmap.GetPixel(testPos.x, testPos.y);
                if (testHeight < minHeight)
                {
                    minHeight = testHeight;
                    gradDir = new Vector2(testPos.x - intPos.x, testPos.y - intPos.y);
                }
            }
        }
        gradDir.Normalize();
        return gradDir;
    }

    private Vector2 GenerateStartPoint(Heightmap heightmap)
    {
        float maxHeight = 0;
        Vector2 startPos = new Vector2(heightmap.getSize() / 2, heightmap.getSize() / 2);

        for (int i = 0; i < 10; i++)
        {
            Vector2 randPos = new Vector2(Random.Range(0, heightmap.getSize()-1), Random.Range(0, heightmap.getSize()-1));

            float height = heightmap.GetPixel(randPos.x, randPos.y);

            if (height > maxHeight)
            {
                maxHeight = height;
                startPos = randPos;
            }
        }
        return startPos;
    }

    private Vector2 FindEndPoint(Heightmap heightmap, Vector2 startPos)
    {
        Vector2 gradDir = Random.insideUnitCircle;
        pos = startPos;

        Vector2 velocity = new Vector2(0, 0);

        for (int i = 0; i < 10000; i++)
        {
            Vector2Int intPos = new Vector2Int((int) pos.x, (int) pos.y);
            gradDir = FindGradient(heightmap, intPos);

            Vector2 prevPos = pos;
            pos += gradDir + velocity * 0.2f;
            velocity = pos - prevPos;

            intPos = new Vector2Int((int) pos.x, (int) pos.y);

            if (isSea(heightmap, intPos))
            {
                return pos;
            }
        }
        Debug.Log("Didnt find an end to a river");
        return pos;
    }

    List<Vector2> Subdivide(List<Vector2> points)
    {
        List<Vector2> newPoints = new List<Vector2>();
        for (int i = 0; i < points.Count - 1; i++)
        {
            Vector2 pointA = points[i];
            Vector2 pointB = points[i+1];

            Vector2 dir = pointB - pointA;
            Vector2 tangent = new Vector2(dir.y, -dir.x);
            tangent.Normalize();
            Vector2 offset = tangent * dir.magnitude * 0.25f;
            if (Random.Range(0, 2) == 0) offset = -offset;

            Vector2 newPoint = pointA + (dir * 0.5f) + offset;

            newPoints.Add(pointA);
            newPoints.Add(newPoint);
        }
        newPoints.Add(points[points.Count-1]);

        return newPoints;
    }

    void TracePoints(Heightmap heightmap, Texture2D colormap, List<Vector2> points)
    {
        pos = points[0];
        for (int i = 0; i < points.Count - 1; i++)
        {
            Vector2 pointA = points[i];
            Vector2 pointB = points[i+1];

            Vector2 dir = pointB - pointA;
            float dist = dir.magnitude;
            dir.Normalize();
            pos = pointA;

            if (dist < 1) dist = 1.001f;
            for (int j = 0; j < (int) dist; j++)
            {
                float height = heightmap.GetPixel((int) pos.x, (int) pos.y);
                heightmap.SetPixel((int) pos.x, (int) pos.y, height - 0.02f);
                colormap.SetPixel((int) pos.x, (int) pos.y, new Color(0.223529f, 0.792157f, 0.8f, 1));
                pos = pos + dir;
            }
        }
    }

    public void Run(Heightmap heightmap, Texture2D colormap)
    {
        if (heightmap == null) return;

        int size = heightmap.getSize();

        OtherSettings otherSettings = GameObject.Find("Terrain").GetComponent<OtherSettings>();

        for (int r = 0; r < otherSettings.numRivers; r++)
        {
            Vector2 startPos = GenerateStartPoint(heightmap);
            Vector2 endPoint = FindEndPoint(heightmap, startPos);

            List<Vector2> points = new List<Vector2>();
            points.Add(startPos);
            points.Add(endPoint);

            for (int i = 0; i < 6; i++)
            {
                points = Subdivide(points);
            }
            
            Debug.Log(points.Count);

            TracePoints(heightmap, colormap, points);
        }
    }
}
