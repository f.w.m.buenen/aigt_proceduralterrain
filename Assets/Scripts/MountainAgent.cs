﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainAgent : MonoBehaviour
{
    private Vector2 pos;
    private float altitude = 1.0f;
    private static System.Random rng = new System.Random();

    float MountainInterpolate(float y1, float y2, float a)
    {
        float v = -Mathf.Pow(a, 0.8f) + 1;
        return v * y1 + (1 - v) * y2;
    }

    private float Clamp(float x, float lowerlimit, float upperlimit)
    {
        if (x < lowerlimit)
            x = lowerlimit;
        if (x > upperlimit)
            x = upperlimit;
        return x;
    }

    bool isSea(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) < 0.01f;
    }

    bool isSea(Heightmap heightmap, float x, float y)
    {
        return isSea(heightmap, new Vector2Int((int) x, (int) y));
    }

    bool isLand(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) >= 0.01f;
    }

    bool isLand(Heightmap heightmap, float x, float y)
    {
        return isLand(heightmap, new Vector2Int((int) x, (int) y));
    }

    void SmoothMountain(Heightmap heightmap, List<Vector2> mountainTops)
    {
        Heightmap tempHeightmap = heightmap.Copy();

        for (int x = 0; x < heightmap.getSize(); x++)
        {
            for (int y = 0; y < heightmap.getSize(); y++)
            {
                Vector2Int p = new Vector2Int(x, y);
                
                if (!isLand(heightmap, p)) continue;

                Vector2Int o = new Vector2Int(x, y);

                float minDist = 1000;
                Vector2Int closestTop = new Vector2Int(0, 0);
                foreach (Vector2 top in mountainTops)
                {
                    o = new Vector2Int((int) top.x, (int) top.y);
                    float pDist = (Vector2Int.Distance(o, p) / heightmap.getSize()) * 4;
                    if (pDist < minDist)
                    {
                        minDist = pDist;
                        closestTop = o;
                    }
                }

                float pixelHeight = heightmap.GetPixel(p.x, p.y);
                float newHeight = System.Math.Max(pixelHeight, MountainInterpolate(altitude, pixelHeight, Clamp(minDist, 0, 1)));
                float oldHeight = tempHeightmap.GetPixel(p.x, p.y);
                float h = System.Math.Max(oldHeight, newHeight);
                tempHeightmap.SetPixel(p.x, p.y, h);
            }
        }

        for (int x = 0; x < heightmap.getSize(); x++)
        {
            for (int y = 0; y < heightmap.getSize(); y++)
            {
                heightmap.SetPixel(x, y, tempHeightmap.GetPixel(x, y));
            }
        }
        Debug.Log(heightmap.GetPixel(256, 256));
    }

    void GenerateMountainKeyPoint(Heightmap heightmap, List<Vector2> keypoints)
    {
        Vector2 start = new Vector2Int(Random.Range(0, heightmap.getSize()), 0);
        Vector2 end = new Vector2Int(Random.Range(0, heightmap.getSize()), heightmap.getSize()-1);

        Vector2 dir = end - start;
        int iterations = (int) dir.magnitude;
        dir.Normalize();

        Vector2 p = start;
        bool onLand = false;
        Vector2 landStart = new Vector2(0, 0);
        Vector2 landEnd = new Vector2(0, 0);
        for (int i = 0; i < iterations; i++)
        {
            if (isLand(heightmap, p.x, p.y) && !onLand)
            {
                onLand = true;
                landStart = p;
            }
            if (isSea(heightmap, p.x, p.y) && onLand)
            {
                onLand = false;
                landEnd = p;
                break;
            }

            p += dir;
        }

        // Threshold the distance from start to end so we dont add a point over a tiny peninsula
        if (Vector2.Distance(landStart, landEnd) < 100) return;

        Vector2 midPoint = (landStart + landEnd) / 2;
        keypoints.Add(midPoint);
    }

    public void Run(Heightmap heightmap)
    {
        if (heightmap == null) return;

        int size = heightmap.getSize();

        pos = new Vector2(size / 2, size / 2);

        OtherSettings otherSettings = GameObject.Find("Terrain").GetComponent<OtherSettings>();

        altitude = otherSettings.mountainHeight;
        List<Vector2> mountainTops = new List<Vector2>();

        for (int i = 0; i < otherSettings.numMountains; i++)
        {
            GenerateMountainKeyPoint(heightmap, mountainTops);
        }

        SmoothMountain(heightmap, mountainTops);

        MountainErosion erosion = new MountainErosion();
        erosion.Run(heightmap, 800000);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
