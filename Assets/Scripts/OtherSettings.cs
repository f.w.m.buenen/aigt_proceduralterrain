﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherSettings : MonoBehaviour
{
    [SerializeField]
    public int numMountains;

    [SerializeField]
    public float mountainHeight;

    [SerializeField]
    public int numRivers;

    [SerializeField]
    public int numBeaches;

    [SerializeField]
    public float beachExtent;

    [SerializeField]
    public float depositionRate;

    [SerializeField]
    public float erosionRate;

    [SerializeField]
    public float particleCapacity;

    [SerializeField]
    public float particleInertia;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
