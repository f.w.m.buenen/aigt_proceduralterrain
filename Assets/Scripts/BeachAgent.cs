﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeachAgent : MonoBehaviour
{
    private static System.Random rng = new System.Random();  

    private Vector2Int pos;

    private float beachHeight = 0.02f;

    float CosineInterpolate(float y1, float y2, float mu)
    {
        float mu2;

        mu2 = (1-Mathf.Cos(mu*Mathf.PI))/2;
        return (y1*(1-mu2)+y2*mu2);
    }

    private float Clamp(float x, float lowerlimit, float upperlimit)
    {
        if (x < lowerlimit)
            x = lowerlimit;
        if (x > upperlimit)
            x = upperlimit;
        return x;
    }

    bool isLand(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) >= 0.01f;
    }

    bool isSea(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) < 0.01f;
    }

    bool isOnCoast(Heightmap heightmap, Vector2Int pos)
    {
        if (isSea(heightmap, new Vector2Int(pos.x + 1, pos.y))) return true;
        if (isSea(heightmap, new Vector2Int(pos.x - 1, pos.y))) return true;
        if (isSea(heightmap, new Vector2Int(pos.x, pos.y + 1))) return true;
        if (isSea(heightmap, new Vector2Int(pos.x, pos.y - 1))) return true;
        return false;
    }

    private static void Shuffle(List<Vector2Int> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            Vector2Int value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    Vector2Int FindCoast(Heightmap heightmap)
    {
        List<Vector2Int> coastLine = new List<Vector2Int>();
        for (int x = 1; x < heightmap.getSize()-2; x++)
        {
            for (int y = 1; y < heightmap.getSize()-2; y++)
            {
                Vector2Int pos = new Vector2Int(x, y);
                if (heightmap.isLand(pos) && isOnCoast(heightmap, pos))
                {
                    float height = heightmap.GetPixel(pos.x, pos.y);
                    if (height < 0.1)
                    {
                        coastLine.Add(pos);
                    }
                }
            }
        }

        if (coastLine.Count > 0)
        {
            Shuffle(coastLine);
            return coastLine[0];
        }

        int maxIterations = 100000;
        Vector2 p = new Vector2(pos.x, pos.y);

        while (maxIterations > 0)
        {
            Vector2 newPos = p + Random.insideUnitCircle;
            Vector2Int newPosI = new Vector2Int((int) newPos.x, (int) newPos.y);
            if (isSea(heightmap, newPosI))
                return new Vector2Int((int) p.x, (int) p.y);
            else
                p = newPos;
            maxIterations--;
        }
        return pos;
    }

    void MoveAlongCoast(Heightmap heightmap)
    {
        List<Vector2Int> lands = new List<Vector2Int>();
        for (int dx = -3; dx <= 3; dx++)
        {
            for (int dy = -3; dy <= 3; dy++)
            {
                if (dx == 0 && dy == 0) continue;
                Vector2Int newPos = pos + new Vector2Int(dx, dy);

                if (newPos.x < 0 || newPos.y < 0 || newPos.x > heightmap.getSize()-1 || newPos.y > heightmap.getSize()-1) continue;

                if (isLand(heightmap, newPos)) lands.Add(newPos);
            }
        }

        Shuffle(lands);
        foreach (Vector2Int land in lands)
        {
            if (isOnCoast(heightmap, land))
            {
                pos = land;
                return;
            }
        }
    }

    void MoveToAdjacentLand(Heightmap heightmap)
    {
        List<Vector2Int> adjacency = new List<Vector2Int>();
        adjacency.Add(new Vector2Int(-1, 0));
        adjacency.Add(new Vector2Int(1, 0));
        adjacency.Add(new Vector2Int(0, -1));
        adjacency.Add(new Vector2Int(0, 1));
        Shuffle(adjacency);

        for (int i = 0; i < 4; i++)
        {
            Vector2Int newPos = pos + adjacency[i];
            if (heightmap.isLand(newPos))
            {
                pos = newPos;
                return;
            }
        }
    }

    void ExpandBeaches(Heightmap heightmap, Texture2D colormap, List<Vector2Int> beaches)
    {
        foreach (Vector2Int beach in beaches)
        {
            pos = beach;

            for (int i = 0; i < 100; i++)
            {
                MoveToAdjacentLand(heightmap);
                heightmap.SetPixel(pos.x, pos.y, beachHeight);
                colormap.SetPixel((int) pos.x, (int) pos.y, new Color(0.8f, 0.8f, 0.4f, 1.0f));
            }

            // for (int x = 0; x < heightmap.getSize(); x++)
            // {
            //     for (int y = 0; y < heightmap.getSize(); y++)
            //     {
            //         Vector2Int p = new Vector2Int(x, y);

            //         float dist = (Vector2Int.Distance(pos, p) / heightmap.getSize()) * Random.Range(8, 12);

            //         if (isLand(heightmap, p) && dist < 0.4f)
            //         {
            //             float pixelHeight = heightmap.GetPixel(p.x, p.y);
            //             float newHeight = height;
            //             heightmap.SetPixel(p.x, p.y, newHeight);
            //             colormap.SetPixel(p.x, p.y, new Color(0.8f, 0.8f, 0.4f, 1.0f));
            //         }
            //     }
            // }
        }
    }

    public void Run(Heightmap heightmap, Texture2D colormap)
    {
        if (heightmap == null) return;

        int size = heightmap.getSize();

        OtherSettings otherSettings = GameObject.Find("Terrain").GetComponent<OtherSettings>();

        for (int b = 0; b < otherSettings.numBeaches; b++)
        {
            pos = FindCoast(heightmap);

            heightmap.SetPixel(pos.x, pos.y, beachHeight);
            List<Vector2Int> beaches = new List<Vector2Int>();
            for (int i = 0; i < otherSettings.beachExtent; i++)
            {
                MoveAlongCoast(heightmap);
                heightmap.SetPixel(pos.x, pos.y, beachHeight);
                beaches.Add(pos);
            }
            ExpandBeaches(heightmap, colormap, beaches);
        }
    }
}
