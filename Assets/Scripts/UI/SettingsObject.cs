﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TerrainSettings", menuName = "TerrainSettings")]
public class SettingsObject : ScriptableObject
{
    [Header("Coastline Agent")]
    [SerializeField]
    public int initLandSize;

    [SerializeField]
    public int numSubdivisions;

    [SerializeField]
    public float agentOverlapRatio;

    [SerializeField]
    public int numInitAgents;

    [SerializeField]
    public int numTokensStart;

    [Header("Peninsula Agent")]
    [SerializeField]
    public int numPeninsulas;

    [SerializeField]
    public int numPeninPathPoints;

    [SerializeField]
    public int pointDistance;

    [SerializeField]
    public int numAgentTokens;

    [SerializeField]
    public float pathAngleVariation;

    [SerializeField]
    public float agentAngleVariation;

    [Header("Mountain Agent")]
    [SerializeField]
    public int numMountains;

    [SerializeField]
    public float mountainHeight;

    [SerializeField]
    public float depositionRate;

    [SerializeField]
    public float erosionRate;

    [SerializeField]
    public float particleCapacity;

    [SerializeField]
    public float particleInertia;

    [Header("River Agent")]
    [SerializeField]
    public int numRivers;

    [Header("Beach Agent")]
    [SerializeField]
    public int numBeaches;

    [SerializeField]
    public int beachExtent;
}
