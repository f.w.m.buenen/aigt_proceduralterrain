﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeninsulaAgent
{
    private CoastlineGenerator coastlineGenerator;
    private int tokens;
    private float radius;
    private Vector2 prefDirection;
    private List<Vector2> borderPart;

    private int tokensRemaining;


    public PeninsulaAgent(CoastlineGenerator coastlineGenerator, int tokens, float radius, Vector2 direction, List<Vector2> borderPart)
    {
        this.coastlineGenerator = coastlineGenerator;
        this.tokens = tokens;
        this.radius = radius;
        this.prefDirection = direction;
        this.borderPart = borderPart;

        this.tokensRemaining = tokens;
    }

    private float Score(Vector2 pos)
    {
        return 1.0f;
    }

    public bool UpdateLand()
    {
        if (tokensRemaining <= 0) return false;

        Vector2 pos = GetRandomPosition();
        if (pos == new Vector2(0, 0))
        {
            tokensRemaining--;
            return true;
        }

        Vector2 initPos = new Vector2(-1, -1);
        Vector2 highestPos = initPos;
        float highestScore = 0.0f;
        // score 
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                Vector2 currPos = new Vector2((int)pos.x + x, (int)pos.y + y);

                if (!PositionInBounds(currPos)) continue;

                if (coastlineGenerator.landBitField[(int)currPos.x, (int)currPos.y]) continue;

                float score = Score(currPos);
                if (score > highestScore || highestPos == initPos)
                {
                    highestScore = score;
                    highestPos = currPos;
                }
            }
        }

        if (highestPos == initPos || pos == Vector2.zero)
        {
            // couldn't find a decent neighbor
            tokensRemaining--;
            return true;
        }

        if (PositionInBounds(highestPos))
        {
            coastlineGenerator.heightmap.SetPixel((int)highestPos.x, (int)highestPos.y, 0.1f);

            coastlineGenerator.landBitField[(int)highestPos.x, (int)highestPos.y] = true;

            coastlineGenerator.borderPoints.Add(highestPos);

            if (!borderPart.Contains(highestPos)) borderPart.Add(highestPos);
            coastlineGenerator.RemoveNonBoundaryPixels(highestPos);
        }
        else Debug.Log("Whoops");

        tokensRemaining--;
        return true;
    }

    private bool PositionInBounds(Vector2 pos)
    {
        return !(pos.x < 0 || pos.y < 0 || pos.x > (coastlineGenerator.TerrainSize - 1) || pos.y > (coastlineGenerator.TerrainSize - 1));
    }

    private bool PositionOnEdge(Vector2 pos)
    {
        return pos.x == 0 || pos.y == 0 || pos.x == (coastlineGenerator.TerrainSize - 1) || pos.y == (coastlineGenerator.TerrainSize - 1);
    }

    public Vector2 GetRandomPosition()
    {
        if (borderPart.Count == 0)
        {
            //Debug.Log("borderpart count is zero!");
            return Vector2.zero;
        }
        int randIndex = Random.Range(0, borderPart.Count);
        Vector2 pos = borderPart[randIndex];

        if (PositionOnEdge(pos)) return Vector2.zero;

        bool inland = true;

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0) continue;

                if (!PositionInBounds(pos + new Vector2(x, y))) continue;

                if (!coastlineGenerator.landBitField[(int)pos.x + x, (int)pos.y + y])
                {
                    inland = false;
                    break;
                }
            }

            if (!inland)
            {
                break;
            }
        }

        if (inland)
        {
            // not a valid border point anymore, remove
            borderPart.Remove(pos);
            coastlineGenerator.borderPoints.Remove(pos);
            Vector2 oldPos = pos;

            // find new border point in preferred direction
            pos = TraverseToCoastline(oldPos);
            //pos = new Vector2(-1, -1);
            if (pos == new Vector2(-1, -1))
            {
                return Vector2.zero;
            }
            else if (!borderPart.Contains(pos)) borderPart.Add(pos);
        }

        return pos;
    }

    public Vector2 TraverseToCoastline(Vector2 startPos)
    {
        Vector2 pos = new Vector2(-1, -1);

        if (PositionOnEdge(startPos)) return pos;

        Vector2 prevPos = pos;
        bool foundCoastPoint = false;
        Vector2 oldPos = startPos;

        int maxCounter = 0;
        do
        {
            Vector2Int newPos = new Vector2Int((int)(oldPos.x + prefDirection.x), (int)(oldPos.y + prefDirection.y));

            if (!PositionInBounds(newPos)) return pos;

            if (!coastlineGenerator.landBitField[newPos.x, newPos.y])
            {
                foundCoastPoint = true;
                pos = prevPos;
            }
            else
            {
                //Debug.Log("not a coastline!");
                //Debug.Log("pref dir x: " + prefDirection.x);
                //Debug.Log("pref dir y: " + prefDirection.y);
            }

            prevPos = newPos;
            oldPos = oldPos + prefDirection;

            //foundCoastPoint = coastlineGenerator.borderPoints.Contains(possibleCoastPoint);

            //if (foundCoastPoint) pos = possibleCoastPoint;
            maxCounter++;
        }
        while (!foundCoastPoint && maxCounter < 100);

        return pos;
    }

    private static Vector2 RotateVector(Vector2 v, float degrees)
    {
        float rad = degrees * Mathf.Deg2Rad;
        float ca = Mathf.Cos(rad);
        float sa = Mathf.Sin(rad);

        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }
}
