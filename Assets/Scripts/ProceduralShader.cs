﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ProceduralShader : MonoBehaviour
{
    [SerializeField]
    private float maxWaterHeight = 0.1f;

    [SerializeField]
    private Gradient waterColor;

    [SerializeField]
    private Gradient terrainColorPerlin;

    [SerializeField]
    private Color cliffColor;

    [SerializeField]
    private float snowHeightMin;

    [SerializeField]
    private float snowHeightMax;

    [SerializeField]
    private float snowFreq;

    [SerializeField]
    private float snowMinExp;

    [SerializeField]
    private float snowMaxExp;

    [SerializeField]
    private float snowAlphaAltitudeExp;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetColors(Mesh mesh, Heightmap heightMap, Texture2D colormap, int subdivisions, int numVerts)
    {
        //Color32[] colors = new Color32[numVerts];
        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        stopwatch.Start();

        Color32[] colors = new Color32[numVerts];
        Vector3[] vertices = mesh.vertices;
        Vector3[] normals = mesh.normals;

        float subdivRecip = 1.0f / (float)(subdivisions + 1);
        int index = -1;
        for (int z = 0; z < subdivisions + 1; z++)
        {
            for (int x = 0; x < subdivisions + 1; x++)
            {
                index++;

                Color color = colormap.GetPixel(x, z);
                if (color != Color.black)
                {
                    colors[index] = color;
                    continue;
                }

                colors[index] = cliffColor;
                float height = vertices[index].y;
                // If vertex height less than water, give water color
                if (height <= maxWaterHeight)
                {
                    float xDiv = (float)x * subdivRecip * 2 + 200;
                    float zDiv = (float)z * subdivRecip * 2 + 300;
                    float perl = Mathf.PerlinNoise(xDiv, zDiv);
                    colors[index] = waterColor.Evaluate(perl);
                    continue;
                }
                if (Vector3.Dot(normals[index], Vector3.up) < 0.5f)
                {
                    colors[index] = cliffColor;
                    continue;
                }
                else
                {
                    float xDiv = (float)x * subdivRecip * 8;
                    float zDiv = (float)z * subdivRecip * 8;
                    float perl = Mathf.PerlinNoise(xDiv, zDiv);
                    colors[index] = terrainColorPerlin.Evaluate(perl);

                    if(height > snowHeightMin)
                    {
                        // TODO: altitude based exponent / frequency
                        float heightDiff = Mathf.Clamp((height - snowHeightMin) / (snowHeightMax - snowHeightMin), 0.0f, 1.0f);
                        float freq = snowFreq;
                        float exp = Mathf.Lerp(snowMaxExp, snowMinExp, heightDiff);

                        xDiv = (float)x * subdivRecip * freq + 1000;
                        zDiv = (float)z * subdivRecip * freq + 1000;

                        float perlVal = Mathf.PerlinNoise(xDiv, zDiv);
                        perl = Mathf.Pow(perlVal, exp) * Mathf.Pow(heightDiff, snowAlphaAltitudeExp);
                        colors[index] = Color.Lerp(colors[index], Color.white, perl);
                    }
                }
            }
        }

        mesh.SetColors(colors);
        stopwatch.Stop();
    }
}
