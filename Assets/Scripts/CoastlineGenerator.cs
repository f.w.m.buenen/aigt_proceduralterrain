﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Heightmap
{
    private float[,] heightmap;
    private int size;

    public Heightmap(int mapSize)
    {
        size = mapSize;
        heightmap = new float[size, size];
    }

    public void SetPixel(int x, int y, float value) { heightmap[x, y] = value; }

    public void SetPixel(float x, float y, float value) { SetPixel((int)x, (int) y, value); }

    public float GetPixel(int x, int y) {
        if (x < 0 || y < 0 || x >= size || y >= size)
        {
            UnityEngine.Debug.Log(x + " " + y);
            System.Diagnostics.StackTrace t = new System.Diagnostics.StackTrace();
            UnityEngine.Debug.Log(t.ToString());
        }
            
        return heightmap[x, y];
    }

    public float GetPixel(float x, float y) {
        return GetPixel((int) x, (int) y);
    }

    public int getSize() { return size; }

    public bool isSea(Vector2Int pos)
    {
        return GetPixel(pos.x, pos.y) < 0.01f;
    }

    public bool isSea(float x, float y)
    {
        return isSea(new Vector2Int((int) x, (int) y));
    }

    public bool isLand(Vector2Int pos)
    {
        return GetPixel(pos.x, pos.y) >= 0.01f;
    }

    public bool isLand(float x, float y)
    {
        return isLand(new Vector2Int((int) x, (int) y));
    }

    public Heightmap Copy()
    {
        Heightmap copy = new Heightmap(size);
        for (int x = 0; x < copy.getSize(); x++)
        {
            for (int y = 0; y < copy.getSize(); y++)
            {
                copy.SetPixel(x, y, GetPixel(x, y));
            }
        }
        return copy;
    }
}

public class CoastlineGenerator : MonoBehaviour
{
    private int terrainSize = 512;
    //private int terrainSize = 256;

    [HideInInspector]
    public Heightmap heightmap;

    [HideInInspector]
    public Texture2D colormap;

    [SerializeField]
    public int numSubDivisions;

    [SerializeField]
    public int actionSize;

    [SerializeField]
    public int actionRadius;

    [SerializeField]
    public int initialLandSize;

    [SerializeField]
    public int numInitialAgents;

    [SerializeField]
    public float actionRadiusDecrease;

    [SerializeField]
    public float overlapRatio;

    //[SerializeField]
    //private int totalLandSize;

    private int sqrActionRadius;

    [HideInInspector]
    public List<Vector2> borderPoints;
    
    [HideInInspector]
    public bool[,] landBitField;

    public int TerrainSize { get => terrainSize; }
    public int ActionRadius { get => actionRadius; }
    public int SqrActionRadius { get => sqrActionRadius; }

    void Start()
    {
        heightmap = new Heightmap(terrainSize);
        colormap = new Texture2D(terrainSize, terrainSize);
        UnityEngine.Debug.Log("terrain size: " + terrainSize);
        
        sqrActionRadius = actionRadius * actionRadius;

        // initialize texture with black pixels
        for (int y = 0; y < heightmap.getSize(); y++)
        {
            for (int x = 0; x < heightmap.getSize(); x++)
            {
                heightmap.SetPixel(x, y, 0);
                colormap.SetPixel(x, y, Color.black);
            }
        }

        landBitField = new bool[terrainSize, terrainSize];

        borderPoints = new List<Vector2>();
    }

    public void StartTerrainGeneration()
    {
        IEnumerator terrainGenProcess = GenerateTerrain();
        StartCoroutine(terrainGenProcess);
    }

    private IEnumerator GenerateTerrain()
    {
        yield return new WaitForEndOfFrame();
        Transform processTextParent = GameObject.Find("CanvasMain/TerrainProcessText").transform;
        GameObject processTextPrefab = Resources.Load("UI/InProcessText") as GameObject;

        GameObject processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Creating initial land...";
        yield return new WaitForEndOfFrame();


        GenerateInitialTerrain(heightmap);

        yield return new WaitForSeconds(0.1f);

        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Applying intial agents...";
        yield return new WaitForEndOfFrame();

        // create subdivisions
        CoastlineAgent[] initialSubdivAgents = new CoastlineAgent[numInitialAgents];
        List<Vector2>[] initialWorkDivision = SubdivideWorkLoad(numInitialAgents, overlapRatio);
        for (int i = 0; i < numInitialAgents; i++)
        {
            int randIndex = UnityEngine.Random.Range(0, initialWorkDivision[i].Count);
            Vector2 randomPoint = initialWorkDivision[i][randIndex];

            initialSubdivAgents[i] = new CoastlineAgent(this, randomPoint, actionSize, initialWorkDivision[i]);
        }
        bool stopSim = false;
        while (!stopSim)
        {
            for (int i = 0; i < numInitialAgents; i++)
            {
                stopSim = stopSim || initialSubdivAgents[i].UpdateLand();
            }

            stopSim = !stopSim;
        }


        yield return new WaitForSeconds(0.1f);

        List<CoastlineAgent> agents = initialSubdivAgents.ToList();
        for (int i = 0; i < numSubDivisions; i++)
        {
            processText = GameObject.Instantiate(processTextPrefab, processTextParent);
            processText.GetComponent<Text>().text = "Applying subdivision " + (i + 1) + "...";
            yield return new WaitForEndOfFrame();

            List<Vector2>[] workload = SubdivideWorkLoad(agents.Count * 2, overlapRatio);

            List<CoastlineAgent> newAgents = new List<CoastlineAgent>();
            for (int a = 0; a < agents.Count; a++)
            {
                Vector2 randomBorderPos1 = agents[a].GetRandomPosition();
                Vector2 randomBorderPos2 = agents[a].GetRandomPosition();
                newAgents.Add(new CoastlineAgent(this, randomBorderPos1, (int)(agents[a].Tokens * 0.5f), workload[a * 2], (int)(agents[a].ActionRadius * (1.0f - actionRadiusDecrease))));
                newAgents.Add(new CoastlineAgent(this, randomBorderPos2, (int)(agents[a].Tokens * 0.5f), workload[a * 2 + 1], (int)(agents[a].ActionRadius * (1.0f - actionRadiusDecrease))));
            }
            agents = newAgents;

            stopSim = false;
            while (!stopSim)
            {
                for (int ix = 0; ix < agents.Count; ix++)
                {
                    stopSim = stopSim || agents[ix].UpdateLand();
                }

                stopSim = !stopSim;
            }

            yield return new WaitForSeconds(0.1f);
        }

        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        SortCoastline();

        PeninsulaGenerator peninGen = GetComponent<PeninsulaGenerator>();
        peninGen.CreatePeninsulas(this);
        processText.GetComponent<Text>().text = "Finished coastline agent!";
        yield return new WaitForEndOfFrame();

        // Smoothing
        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Smoothing heightmap...";
        yield return new WaitForEndOfFrame();
        Heightmap filteredMap = heightmap;
        int k = 3;
        for (int x = k; x < heightmap.getSize()-k; x++)
        {
            for (int y = k; y < heightmap.getSize()-k; y++)
            {
                float totalValue = 0;
                for (int dx = -k; dx <= k; dx++)
                {
                    for (int dy = -k; dy <= k; dy++)
                    {
                        totalValue += heightmap.GetPixel(x+dx, y+dy);
                    }
                }
                totalValue /= (k+k+1) * (k+k+1);
                filteredMap.SetPixel(x, y, totalValue);
            }
        }
        heightmap = filteredMap;

        // Mountain agent
        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Running mountain agent..";
        yield return new WaitForEndOfFrame();
        MountainAgent mount = new MountainAgent();
        mount.Run(heightmap);

        // Cliff agent
        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Running cliff agent..";
        yield return new WaitForEndOfFrame();
        CliffAgent cliffAgent = new CliffAgent();
        cliffAgent.Run(heightmap);

        // River agent
        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Running river agent..";
        yield return new WaitForEndOfFrame();
        RiverAgent riverAgent = new RiverAgent();
        riverAgent.Run(heightmap, colormap);

        // Beach agent
        processText = GameObject.Instantiate(processTextPrefab, processTextParent);
        processText.GetComponent<Text>().text = "Running beach agent..";
        yield return new WaitForEndOfFrame();
        BeachAgent beachAgent = new BeachAgent();
        beachAgent.Run(heightmap, colormap);

        yield return new WaitForEndOfFrame();
        this.GetComponent<MeshGenerator>().UpdateMesh(heightmap, colormap);
        // use to visualize the boundary
        //foreach (Vector2 position in borderPoints)
        //{
        //    coastText.SetPixel((int)position.x, (int)position.y, Color.red);
        //}
    }

    private void GenerateInitialTerrain(Heightmap heightmap)
    {
        // create first landpoint in the middle of the texture
        Vector2 startPoint = new Vector2(heightmap.getSize() / 2, heightmap.getSize() / 2);
        heightmap.SetPixel((int)startPoint.x, (int)startPoint.y, 0.05f);
        landBitField[(int)startPoint.x, (int)startPoint.y] = true;
        borderPoints.Add(startPoint);

        // create initial land
        CoastlineAgent initAgent = new CoastlineAgent(this, startPoint, initialLandSize, borderPoints, 120);
        bool stopSim = false;
        while (!stopSim)
        {
            stopSim = !initAgent.UpdateLand();
        }
        
        // use to visualize the boundary
        //foreach (Vector2 position in borderPoints)
        //{
        //    coastText.SetPixel((int)position.x, (int)position.y, Color.red);
        //}
    }

    public void RemoveNonBoundaryPixels(Vector2 newBoundaryPoint)
    {
        for (int y = -1; y <= 1; y++)
        {
            for (int x = -1; x <= 1; x++)
            {
                if (x == 0 && y == 0) continue;

                Vector2 neighborPos = new Vector2(newBoundaryPoint.x + x, newBoundaryPoint.y + y);
                if (neighborPos.x < 0 || neighborPos.x >= TerrainSize) continue;
                if (neighborPos.y < 0 || neighborPos.y >= TerrainSize) continue;

                int boundaryIndex = borderPoints.IndexOf(neighborPos);

                if (!landBitField[(int)neighborPos.x, (int)neighborPos.y] || boundaryIndex == -1) continue;

                bool shouldRemove = true;
                for (int ys = -1; ys <= 1; ys++)
                {
                    for(int xs = -1; xs <= 1; xs++)
                    {
                        if ((neighborPos.x + xs) < 0 || (neighborPos.x + xs) >= TerrainSize) continue;
                        if ((neighborPos.y + ys) < 0 || (neighborPos.y + ys) >= TerrainSize) continue;
                        
                        if(!landBitField[(int)neighborPos.x + xs, (int)neighborPos.y + ys])
                        {
                            shouldRemove = false;
                            break;
                        }
                    }

                    if(!shouldRemove)
                    {
                        break;
                    }
                }

                if(shouldRemove)
                {
                    borderPoints.RemoveAt(boundaryIndex);
                }
            }

        }
    }

    private List<Vector2>[] SubdivideWorkLoad(int numAgents, float overlapPercentage)
    {
        SortCoastline();
        
        List<Vector2>[] workDivision = new List<Vector2>[numAgents];
        int numCoastlinePoints = borderPoints.Count;
        int numPerAgent = borderPoints.Count / numAgents;
        int overlap = (int)(numPerAgent * overlapPercentage);
        int numPerAgentTotal = numPerAgent + overlap * 2;

        for(int i = 0; i < numAgents; i++)
        {
            workDivision[i] = new List<Vector2>();
            int startIdx = i * numPerAgent - overlap;
            for(int idx = 0; idx < numPerAgentTotal; idx++)
            {

                int borderIndex = (startIdx + idx) % borderPoints.Count;
                if(borderIndex < 0)
                {
                    borderIndex = borderIndex + borderPoints.Count;
                }
                
                workDivision[i].Add(borderPoints[borderIndex]);
            }
        }

        return workDivision;
    }

    // lays out the border points in sequential order (counter clock-wise)
    private void SortCoastline()
    {
        List<Vector2> newOrderedBorderPoints = new List<Vector2>();

        // find starting point (outer right point)
        Vector2 startPoint = borderPoints[0];
        foreach (Vector2 point in borderPoints)
        {
            if (point.x > startPoint.x) startPoint = point;
        }

        Vector2 currentPoint = startPoint;
        Vector2 nextPoint = new Vector2();
        for(int i = 0; i < borderPoints.Count; i++)
        {
            bool borderPointFound = false;
            int traceBackCounter = 0;
            do
            {
                nextPoint = GetNextBorderPoint(currentPoint, newOrderedBorderPoints);

                borderPointFound = nextPoint != new Vector2(-1, -1);
                if(!borderPointFound)
                {
                    if ((newOrderedBorderPoints.Count - 1 - traceBackCounter) < 0)
                    {
                        //Debug.LogError("Something went terribly wrong.");
                        return;
                    }
                    currentPoint = newOrderedBorderPoints[newOrderedBorderPoints.Count - 1 - traceBackCounter];
                    traceBackCounter++;
                    //Debug.Log("BLAH");
                }
            }
            while (!borderPointFound);

            newOrderedBorderPoints.Add(nextPoint);
        }

        borderPoints = newOrderedBorderPoints;
    }

    private bool CheckIsNextOnBorder(Vector2 newPos, List<Vector2> orderedList)
    {
        if (newPos.x < 0 || newPos.x >= TerrainSize) return false;
        if (newPos.y < 0 || newPos.y >= TerrainSize) return false;

        // no land, so it cannot be the next point on the border (lowers number of O(n) find operations)
        if (!landBitField[(int) newPos.x, (int) newPos.y]) return false;

        // not on border, so skip this one 
        if (!borderPoints.Contains(newPos)) return false;

        // we have already seen this border point
        if (orderedList.Contains(newPos)) return false;

        return true;
    }

    private Vector2 GetNextBorderPoint(Vector2 pos, List<Vector2> newOrderedBorderPoints)
    {
        Vector2[] neighbourhood = 
        {
            new Vector2(0, 1),
            new Vector2(-1, 0),
            new Vector2(0, -1),
            new Vector2(1, 0),
            new Vector2(-1, 1),
            new Vector2(-1, -1),
            new Vector2(1, -1),
            new Vector2(1, 1)
        };

        foreach (Vector2 dir in neighbourhood)
        {
            if (CheckIsNextOnBorder(pos + dir, newOrderedBorderPoints))
                return pos + dir;
        }

        return new Vector2(-1, -1);
    }
}
