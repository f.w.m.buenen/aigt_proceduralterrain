﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle
{
    public Vector2 pos;
    public Vector2 dir = new Vector2(0, 0);
    public float vel = 1;

    public float water = 1;

    public float sediment = 0;

    public Particle(Vector2 _pos)
    {
        pos = _pos;
    }
}

public class MountainErosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Vector2 FindGradient(Heightmap map, Vector2 pos)
    {
        if (pos.x < 0 || pos.y < 0 || pos.x >= map.getSize() - 2 || pos.y >= map.getSize() - 2)
            return new Vector2(0, 0);
        
        int x = (int) pos.x;
        int y = (int) pos.y;

        float u = pos.x - x;
        float v = pos.y - y;

        float x1y1 = map.GetPixel(x, y);
        float x2y1 = map.GetPixel(x+1, y);
        float x1y2 = map.GetPixel(x, y+1);
        float x2y2 = map.GetPixel(x+1, y+1);

        Vector2 grad = new Vector2(
            (x2y1 - x1y1) * (1-v) + (x2y2 - x1y2) * v,
            (x1y2 - x1y1) * (1-u) + (x2y2 - x2y1) * u
        );

        return grad;
    }

    float SampleMapBilinear(Heightmap map, Vector2 p)
    {
        if (p.x < 0 || p.y < 0 || p.x >= map.getSize() - 2 || p.y >= map.getSize() - 2)
            return 0;
        
        Vector2Int intPos = new Vector2Int((int) p.x, (int) p.y);
        Vector2 uv = p - intPos;

        float x1y1 = map.GetPixel(intPos.x, intPos.y);
        float x2y1 = map.GetPixel(intPos.x+1, intPos.y);
        float x1y2 = map.GetPixel(intPos.x, intPos.y+1);
        float x2y2 = map.GetPixel(intPos.x+1, intPos.y+1);

        return x1y1 * (1 - uv.x) * (1 - uv.y) + x2y1 * uv.x * (1-uv.y) + x1y2 * (1-uv.x) * uv.y + x2y2 * uv.x * uv.y;
    }

    public void BilinearDeposit(Heightmap map, Particle p, float sediment)
    {
        if (map.isSea(p.pos.x, p.pos.y)) return;
        if (p.pos.x < 0 || p.pos.y < 0 || p.pos.x >= map.getSize() - 2 || p.pos.y >= map.getSize() - 2)
            return;

        Vector2Int intPos = new Vector2Int((int) p.pos.x, (int) p.pos.y);

        Vector2 uv = p.pos - intPos;

        map.SetPixel(intPos.x, intPos.y, map.GetPixel(intPos.x, intPos.y) + sediment * (1-uv.x) * (1-uv.y));
        map.SetPixel(intPos.x+1, intPos.y, map.GetPixel(intPos.x+1, intPos.y) + sediment * uv.x * (1-uv.y));
        map.SetPixel(intPos.x, intPos.y+1, map.GetPixel(intPos.x, intPos.y+1) + sediment * (1-uv.x) * uv.y);
        map.SetPixel(intPos.x+1, intPos.y+1, map.GetPixel(intPos.x+1, intPos.y+1) + sediment * uv.x * uv.y);
    }

    public void Erode(Heightmap map, Particle p, float takenSediment)
    {
        int radius = 2;

        Vector2Int intPos = new Vector2Int((int) p.pos.x, (int) p.pos.y);
        float weightSum = 0;
        for (int dx = -radius; dx <= radius; dx++)
        {
            for (int dy = -radius; dy <= radius; dy++)
            {
                Vector2Int ePos = intPos + new Vector2Int(dx, dy);
                if (ePos.x < 0 || ePos.y < 0 || ePos.x > map.getSize() - 1 || ePos.y > map.getSize() - 1) continue;

                Vector2 diff = new Vector2(ePos.x - p.pos.x, ePos.y - p.pos.y);
                float dist = diff.magnitude;
                if (dist >= radius) continue;
                float weight = 1 - dist / radius;
                weightSum += weight;
            }
        }
        for (int dx = -radius; dx <= radius; dx++)
        {
            for (int dy = -radius; dy <= radius; dy++)
            {
                Vector2Int ePos = intPos + new Vector2Int(dx, dy);
                if (ePos.x < 0 || ePos.y < 0 || ePos.x > map.getSize() - 1 || ePos.y > map.getSize() - 1) continue;

                Vector2 diff = new Vector2(ePos.x - p.pos.x, ePos.y - p.pos.y);
                float dist = diff.magnitude;

                if (dist >= radius) continue;
                float weight = (1 - dist / radius) / weightSum;

                float terrainHeight = map.GetPixel(ePos.x, ePos.y);
                float sedimentToTake = takenSediment * weight > terrainHeight ? terrainHeight : takenSediment * weight;
                map.SetPixel(ePos.x, ePos.y, map.GetPixel(ePos.x, ePos.y) - sedimentToTake);
                p.sediment += takenSediment * weight;
            }
        }
    }

    public void Run(Heightmap map, int iterations)
    {
        OtherSettings otherSettings = GameObject.Find("Terrain").GetComponent<OtherSettings>();
        
        float pMinSlope = 0;
        float pDeposition = otherSettings.depositionRate;
        float pErosion = otherSettings.erosionRate;
        float pCapacity = otherSettings.particleCapacity;
        float pGravity = 4.0f;
        float pEvaporation = 0.01f;
        float pInertia = otherSettings.particleInertia;

        for (int i = 0; i < iterations; i++)
        {
            Vector2 randomPos = new Vector2(Random.Range(0, map.getSize()-1), Random.Range(0, map.getSize()-1));
            Particle p = new Particle(randomPos);

            for (int life = 0; life < 30; life++)
            {
                // Compute gradient
                Vector2Int intPos = new Vector2Int((int) p.pos.x, (int) p.pos.y);
                Vector2 uv = p.pos - intPos;
                
                float oldHeight = SampleMapBilinear(map, p.pos);

                Vector2 grad = FindGradient(map, p.pos);
                p.dir = p.dir * pInertia - grad * (1 - pInertia);
                if (p.dir.sqrMagnitude < 0.0001f) p.dir = Random.insideUnitCircle;
                p.dir.Normalize();
                
                Vector2 newPos = p.pos + p.dir;

                // If particle outside of the map, cut it off
                if (map.isSea(p.pos.x, p.pos.y)) break;
                if (newPos.x < 0 || newPos.x > map.getSize()-1 || newPos.y < 0 || newPos.y > map.getSize()-1) break;

                float newHeight = SampleMapBilinear(map, newPos);
                float diffHeight = newHeight - oldHeight;

                // Compute sediment capacity
                float c = Mathf.Max(-diffHeight * p.vel * p.water * pCapacity, 0.01f);

                // We carry more sediment than our capacity or we go uphill
                if (p.sediment > c || diffHeight > 0)
                {
                    float droppedSediment = (diffHeight > 0) ? Mathf.Min(diffHeight, p.sediment) : (p.sediment - c) * pDeposition;
                    p.sediment -= droppedSediment;
                    BilinearDeposit(map, p, droppedSediment);
                }
                // The drop carries less than its capacity, take some from the terrain
                else
                {
                    float takenSediment = Mathf.Min((c - p.sediment) * pErosion, -diffHeight);
                    Erode(map, p, takenSediment);
                }

                p.pos = newPos;

                // Geometric mean of squared old speed and height difference
                p.vel = Mathf.Sqrt(p.vel * p.vel + diffHeight * pGravity);

                p.water = p.water * (1 - pEvaporation);
            }
        }
    }
}
