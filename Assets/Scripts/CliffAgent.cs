﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CliffAgent : ScriptableObject
{
    private static System.Random rng = new System.Random();  

    private Vector2Int pos;

    private float cliffHeight = 0.4f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    float CosineInterpolate(float y1, float y2, float mu)
    {
        float mu2;

        mu2 = (1-Mathf.Cos(mu*Mathf.PI))/2;
        return (y1*(1-mu2)+y2*mu2);
    }

    private float Clamp(float x, float lowerlimit, float upperlimit)
    {
        if (x < lowerlimit)
            x = lowerlimit;
        if (x > upperlimit)
            x = upperlimit;
        return x;
    }

    private static void Shuffle(List<Vector2Int> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            Vector2Int value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    bool isLand(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) >= 0.01f;
    }

    bool isSea(Heightmap heightmap, Vector2Int pos)
    {
        return heightmap.GetPixel(pos.x, pos.y) < 0.01f;
    }

    bool isOnCoast(Heightmap heightmap, Vector2Int pos)
    {
        if (isSea(heightmap, new Vector2Int(pos.x + 1, pos.y))) return true;
        if (isSea(heightmap, new Vector2Int(pos.x - 1, pos.y))) return true;
        if (isSea(heightmap, new Vector2Int(pos.x, pos.y + 1))) return true;
        if (isSea(heightmap, new Vector2Int(pos.x, pos.y - 1))) return true;
        return false;
    }

    Vector2Int FindCoast(Heightmap heightmap)
    {
        int maxIterations = 100000;
        Vector2 p = new Vector2(pos.x, pos.y);

        while (maxIterations > 0)
        {
            Vector2 newPos = p + Random.insideUnitCircle;
            Vector2Int newPosI = new Vector2Int((int) newPos.x, (int) newPos.y);
            if (isSea(heightmap, newPosI))
                return new Vector2Int((int) p.x, (int) p.y);
            else
                p = newPos;
            maxIterations--;
        }
        return pos;
    }

    void MoveAlongCoast(Heightmap heightmap)
    {
        List<Vector2Int> lands = new List<Vector2Int>();
        for (int dx = -3; dx <= 3; dx++)
        {
            for (int dy = -3; dy <= 3; dy++)
            {
                if (dx == 0 && dy == 0) continue;
                Vector2Int newPos = pos + new Vector2Int(dx, dy);
                if (isLand(heightmap, newPos)) lands.Add(newPos);
            }
        }

        Shuffle(lands);
        foreach (Vector2Int land in lands)
        {
            if (isOnCoast(heightmap, land))
            {
                pos = land;
                return;
            }
        }
    }

    void SmoothCliffs(Heightmap heightmap, List<Vector2Int> cliffs)
    {
        foreach (Vector2Int cliff in cliffs)
        {
            pos = cliff;
            float height = cliffHeight;

            for (int x = 0; x < heightmap.getSize(); x++)
            {
                for (int y = 0; y < heightmap.getSize(); y++)
                {
                    Vector2Int p = new Vector2Int(x, y);

                    float dist = (Vector2Int.Distance(pos, p) / heightmap.getSize()) * 2;

                    if (isLand(heightmap, p))
                    {
                        float pixelHeight = heightmap.GetPixel(p.x, p.y);
                        float newHeight = System.Math.Max(pixelHeight, CosineInterpolate(pixelHeight, height, 1-Clamp(dist*2, 0, 1)));
                        heightmap.SetPixel(p.x, p.y, newHeight);
                    }
                }
            }
        }
    }

    public void Run(Heightmap heightmap)
    {
        if (heightmap == null) return;

        int size = heightmap.getSize();

        pos = new Vector2Int(size / 2, size / 2);

        pos = FindCoast(heightmap);

        heightmap.SetPixel(pos.x, pos.y, cliffHeight);
        List<Vector2Int> cliffs = new List<Vector2Int>();
        for (int i = 0; i < 1; i++)
        {
            MoveAlongCoast(heightmap);
            heightmap.SetPixel(pos.x, pos.y, cliffHeight);
            cliffs.Add(pos);
        }
        SmoothCliffs(heightmap, cliffs);
    }
}
