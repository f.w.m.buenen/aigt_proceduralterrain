﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using UnityEngine;

// TODO:  make parameters ranged for variety of different peninsulas
public class PeninsulaGenerator : MonoBehaviour
{
    [SerializeField]
    public int numPeninsulas;

    [SerializeField]
    public int numPointsPerPeninsula;

    [SerializeField]
    public int pointDistance;

    [SerializeField]
    public int numLandPointsPerPeninPoint;

    [SerializeField]
    public float agentAngleVariation;

    [SerializeField]
    public float peninsulaAngleVariation;

    CoastlineGenerator coastlineGenerator;

    public void CreatePeninsulas(CoastlineGenerator coastlineGenerator)
    {
        this.coastlineGenerator = coastlineGenerator;
        List<Vector2> mainLand = coastlineGenerator.borderPoints;

        for(int i = 0; i < numPeninsulas; i++)
        {
            int startIndex = Random.Range(0, mainLand.Count);

            int leftIndex = startIndex - 1;
            if (leftIndex < 0) leftIndex += (mainLand.Count - 1);
            int rightIndex = (startIndex + 1) % (mainLand.Count - 1);

            Vector2 tangent = (mainLand[rightIndex] - mainLand[leftIndex]);
            Vector2 direction = new Vector2(-tangent.y, tangent.x);
            Vector2 startPoint = mainLand[startIndex];

            float magnitude = Mathf.Sqrt(direction.x * direction.x + direction.y * direction.y);
            direction.x /= magnitude;
            direction.y /= magnitude;

            float nextX = startPoint.x, nextY = startPoint.y;
            for (int p = 0; p < numPointsPerPeninsula; p++)
            {
                float angleDeviation = Random.Range(0.0f, peninsulaAngleVariation);
                Vector2 nextDirection = RotateVector(direction, angleDeviation);

                nextX += nextDirection.x * pointDistance;
                nextY += nextDirection.y * pointDistance;
                int indexX = (int)nextX;
                int indexY = (int)nextY;
                if (indexX < 0 || indexX >= coastlineGenerator.TerrainSize) break;
                if (indexY < 0 || indexY >= coastlineGenerator.TerrainSize) break;

                coastlineGenerator.landBitField[indexX, indexY] = true;
                coastlineGenerator.heightmap.SetPixel(indexX, indexY, 0.1f);

                Vector2 leftDir = new Vector2(-nextDirection.y, nextDirection.x);
                Vector2 rightDir = -leftDir;

                // apply some variation to preferred direction
                float leftAngleDeviation = Random.Range(0.0f, agentAngleVariation);
                float rightAngleDeviation = Random.Range(0.0f, agentAngleVariation);

                leftDir = RotateVector(leftDir, leftAngleDeviation);
                rightDir = RotateVector(rightDir, rightAngleDeviation);

                List<Vector2> border = new List<Vector2>() { new Vector2(nextX, nextY) };
                PeninsulaAgent leftAgent = new PeninsulaAgent(coastlineGenerator, numLandPointsPerPeninPoint, 100.0f, leftDir, border);
                PeninsulaAgent rightAgent = new PeninsulaAgent(coastlineGenerator, numLandPointsPerPeninPoint, 100.0f, rightDir, border);

                bool shouldStop = false;
                while (!shouldStop)
                {
                    shouldStop = !leftAgent.UpdateLand() && !rightAgent.UpdateLand();
                }
            }
        }
    }

    private bool PositionInBounds(Vector2 pos)
    {
        return !(pos.x < 0 || pos.y < 0 || pos.x > (coastlineGenerator.TerrainSize - 1) || pos.y > (coastlineGenerator.TerrainSize - 1));
    }

    private bool PositionOnEdge(Vector2 pos)
    {
        return pos.x == 0 || pos.y == 0 || pos.x == (coastlineGenerator.TerrainSize - 1) || pos.y == (coastlineGenerator.TerrainSize - 1);
    }


    public Vector2 TraverseToCoastline(Vector2 startPos, Vector2 direction)
    {
        Vector2 pos = new Vector2(-1, -1);

        if (PositionOnEdge(startPos)) return pos;

        Vector2 prevPos = pos;
        bool foundCoastPoint = false;
        Vector2 oldPos = startPos;

        int maxCounter = 0;
        do
        {
            Vector2Int newPos = new Vector2Int((int)(oldPos.x + direction.x), (int)(oldPos.y + direction.y));

            if (!PositionInBounds(newPos)) return pos;

            if (!coastlineGenerator.landBitField[newPos.x, newPos.y])
            {
                foundCoastPoint = true;
                pos = prevPos;
            }

            prevPos = newPos;
            oldPos = oldPos + direction;
            maxCounter++;
        }
        while (!foundCoastPoint && maxCounter < 100);

        return pos;
    }

    private static Vector2 RotateVector(Vector2 v, float degrees)
    {
        float rad = degrees * Mathf.Deg2Rad;
        float ca = Mathf.Cos(rad);
        float sa = Mathf.Sin(rad);

        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }
}
