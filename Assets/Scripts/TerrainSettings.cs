﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TerrainSettings : MonoBehaviour
{
    [Header("Terrain")]
    [SerializeField]
    private GameObject terrainObject;

    [Header("Settings")]
    // settings that are used for the terrain
    [SerializeField]
    private SettingsObject storedSettings;
    
    // a default set of settings
    [SerializeField]
    private SettingsObject defaultSettings;

    [Header("Coastline Agent")]
    [Header("----- UI -----")]
    [SerializeField]
    public InputField initLandSize_ui;

    [SerializeField]
    public InputField numSubdivisions_ui;

    [SerializeField]
    public InputField agentOverlapRatio_ui;

    [SerializeField]
    public InputField numInitAgents_ui;

    [SerializeField]
    public InputField numTokensStart_ui;

    [Header("Peninsula Agent")]
    [SerializeField]
    public InputField numPeninsulas_ui;

    [SerializeField]
    public InputField numPeninPathPoints_ui;

    [SerializeField]
    public InputField pointDistance_ui;

    [SerializeField]
    public InputField numAgentTokens_ui;

    [SerializeField]
    public InputField pathAngleVariation_ui;

    [SerializeField]
    public InputField agentAngleVariation_ui;

    [Header("Mountain Agent")]
    [SerializeField]
    public InputField numMountains_ui;

    [SerializeField]
    public InputField mountainHeight_ui;

    [Header("Mountain Erosion")]
    [SerializeField]
    public InputField depositionRate_ui;

    [SerializeField]
    public InputField erosionRate_ui;

    [SerializeField]
    public InputField particleCapacity_ui;

    [SerializeField]
    public InputField particleInertia_ui;


    [Header("River Agent")]
    [SerializeField]
    public InputField numRivers_ui;

    [Header("Beach Agent")]
    [SerializeField]
    public InputField numBeaches_ui;

    [SerializeField]
    public InputField beachExtent_ui;

    [Header("Buttons")]
    [SerializeField]
    private Button generateButton;
    [SerializeField]
    private Button quitButton;
    [SerializeField]
    private Button restoreButton;
    [SerializeField]
    private Button restartButton;

    [Header("Canvas")]
    [SerializeField]
    private GameObject settingsCanvas;

    [SerializeField]
    private GameObject terrainCanvas;

    // Start is called before the first frame update
    void Start()
    {
        generateButton.onClick.AddListener(TriggerTerrainGeneration);
        quitButton.onClick.AddListener(QuitApplication);
        restoreButton.onClick.AddListener(RestoreDefaults);
        restartButton.onClick.AddListener(Restart);

        terrainCanvas.SetActive(false);

        // setup settings
        InitializeSettings();
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    private void InitializeSettings()
    {
        // coastline agent
        initLandSize_ui.text = storedSettings.initLandSize.ToString();
        numSubdivisions_ui.text = storedSettings.numSubdivisions.ToString();
        agentOverlapRatio_ui.text = storedSettings.agentOverlapRatio.ToString();
        numInitAgents_ui.text = storedSettings.numInitAgents.ToString();
        numTokensStart_ui.text = storedSettings.numTokensStart.ToString();

        // peninsula agent
        numPeninsulas_ui.text = storedSettings.numPeninsulas.ToString();
        numPeninPathPoints_ui.text = storedSettings.numPeninPathPoints.ToString();
        pointDistance_ui.text = storedSettings.pointDistance.ToString();
        numAgentTokens_ui.text = storedSettings.numAgentTokens.ToString();
        pathAngleVariation_ui.text = storedSettings.pathAngleVariation.ToString();
        agentAngleVariation_ui.text = storedSettings.agentAngleVariation.ToString();

        // Mountain agent
        numMountains_ui.text = storedSettings.numMountains.ToString();
        mountainHeight_ui.text = storedSettings.mountainHeight.ToString();
        depositionRate_ui.text = storedSettings.depositionRate.ToString();
        erosionRate_ui.text = storedSettings.erosionRate.ToString();
        particleCapacity_ui.text = storedSettings.particleCapacity.ToString();
        particleInertia_ui.text = storedSettings.particleInertia.ToString();

        // River agent
        numRivers_ui.text = storedSettings.numRivers.ToString();

        // Beach agent
        numBeaches_ui.text = storedSettings.numBeaches.ToString();
        beachExtent_ui.text = storedSettings.beachExtent.ToString();

        Debug.Log("settings initialized");
    }

    private void TriggerTerrainGeneration()
    {
        settingsCanvas.SetActive(false);
        terrainCanvas.SetActive(true);

        StoreSettings();
        ApplySettings();
        // TODO: hide canvas, show progress canvas
        terrainObject.GetComponent<CoastlineGenerator>().StartTerrainGeneration();
    }

    private void StoreSettings()
    {
        // coastline agent
        storedSettings.initLandSize = int.Parse(initLandSize_ui.text);
        storedSettings.numSubdivisions = int.Parse(numSubdivisions_ui.text);
        storedSettings.agentOverlapRatio = float.Parse(agentOverlapRatio_ui.text);
        storedSettings.numInitAgents = int.Parse(numInitAgents_ui.text);
        storedSettings.numTokensStart = int.Parse(numTokensStart_ui.text);

        // peninsula agent
        storedSettings.numPeninsulas = int.Parse(numPeninsulas_ui.text);
        storedSettings.numPeninPathPoints = int.Parse(numPeninPathPoints_ui.text);
        storedSettings.pointDistance = int.Parse(pointDistance_ui.text);
        storedSettings.numAgentTokens = int.Parse(numAgentTokens_ui.text);
        storedSettings.pathAngleVariation = float.Parse(pathAngleVariation_ui.text);
        storedSettings.agentAngleVariation = float.Parse(agentAngleVariation_ui.text);

        // Mountain agent
        storedSettings.numMountains = int.Parse(numMountains_ui.text);
        storedSettings.mountainHeight = float.Parse(mountainHeight_ui.text);
        storedSettings.depositionRate = float.Parse(depositionRate_ui.text);
        storedSettings.erosionRate =  float.Parse(erosionRate_ui.text);
        storedSettings.particleCapacity = float.Parse(particleCapacity_ui.text);
        storedSettings.particleInertia = float.Parse(particleInertia_ui.text);

        // River agent
        storedSettings.numRivers = int.Parse(numRivers_ui.text);

        // Beach agent
        storedSettings.numBeaches = int.Parse(numBeaches_ui.text);
        storedSettings.beachExtent = int.Parse(beachExtent_ui.text);
    }

    private void ApplySettings()
    {
        // apply coastline generation settings
        CoastlineGenerator coastlineGen = terrainObject.GetComponent<CoastlineGenerator>();
        coastlineGen.initialLandSize = storedSettings.initLandSize;
        coastlineGen.numSubDivisions = storedSettings.numSubdivisions;
        coastlineGen.overlapRatio = storedSettings.agentOverlapRatio;
        coastlineGen.numInitialAgents = storedSettings.numInitAgents;
        coastlineGen.actionSize = storedSettings.numTokensStart;

        // apply peninsula generation settings
        PeninsulaGenerator peninGen = terrainObject.GetComponent<PeninsulaGenerator>();
        peninGen.numPeninsulas = storedSettings.numPeninsulas;
        peninGen.numPointsPerPeninsula = storedSettings.numPeninPathPoints;
        peninGen.pointDistance = storedSettings.pointDistance;
        peninGen.numLandPointsPerPeninPoint = storedSettings.numAgentTokens;
        peninGen.peninsulaAngleVariation = storedSettings.pathAngleVariation;
        peninGen.agentAngleVariation = storedSettings.agentAngleVariation;

        // apply other settings
        OtherSettings otherSettings = terrainObject.GetComponent<OtherSettings>();
        otherSettings.numMountains = storedSettings.numMountains;
        otherSettings.mountainHeight = storedSettings.mountainHeight;
        otherSettings.numRivers = storedSettings.numRivers;
        otherSettings.numBeaches = storedSettings.numBeaches;
        otherSettings.beachExtent = storedSettings.beachExtent;
        otherSettings.depositionRate = storedSettings.depositionRate;
        otherSettings.erosionRate = storedSettings.erosionRate;
        otherSettings.particleCapacity = storedSettings.particleCapacity;
        otherSettings.particleInertia = storedSettings.particleInertia;
    }

    private void RestoreDefaults()
    {
        // coastline agent
        storedSettings.initLandSize = defaultSettings.initLandSize;
        storedSettings.numSubdivisions = defaultSettings.numSubdivisions;
        storedSettings.agentOverlapRatio = defaultSettings.agentOverlapRatio;
        storedSettings.numInitAgents = defaultSettings.numInitAgents;
        storedSettings.numTokensStart = defaultSettings.numTokensStart;

        // peninsula agent
        storedSettings.numPeninsulas = defaultSettings.numPeninsulas;
        storedSettings.numPeninPathPoints = defaultSettings.numPeninPathPoints;
        storedSettings.pointDistance = defaultSettings.pointDistance;
        storedSettings.numAgentTokens = defaultSettings.numAgentTokens;
        storedSettings.pathAngleVariation = defaultSettings.pathAngleVariation;
        storedSettings.agentAngleVariation = defaultSettings.agentAngleVariation;

        // other settings
        storedSettings.numMountains = defaultSettings.numMountains;
        storedSettings.mountainHeight = defaultSettings.mountainHeight;
        storedSettings.numRivers = defaultSettings.numRivers;
        storedSettings.numBeaches = defaultSettings.numBeaches;
        storedSettings.beachExtent = defaultSettings.beachExtent;
        storedSettings.depositionRate = defaultSettings.depositionRate;
        storedSettings.erosionRate = defaultSettings.erosionRate;
        storedSettings.particleCapacity = defaultSettings.particleCapacity;
        storedSettings.particleInertia = defaultSettings.particleInertia;

        // apply stored settings to UI
        InitializeSettings();
    }

    private void QuitApplication()
    {
        Application.Quit();
    }
}
